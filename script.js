const categories = [];

async function importHeader() {
    const headerContainer = document.querySelector('.header-container');

    return new Promise((resolve, reject) => {
        fetch('/header.html')
            .then(response => response.text())
            .then(data => {
                headerContainer.innerHTML = data;

                resolve();
            })
            .catch(error => {
                console.error(error)

                reject(error)
            });
    })
}

async function importFooter() {
    const footerContainer = document.querySelector('.footer-container');

    return new Promise((resolve, reject) => {
        fetch('/footer.html')
            .then(response => response.text())
            .then(data => {
                footerContainer.innerHTML = data;

                resolve();
            })
            .catch(error => {
                console.error(error)

                reject(error)
            });
    })
}

async function importPopup () {
    const popupContainer = document.querySelector('.popup-container');

    return new Promise((resolve, reject) => {
        fetch('/order-popup.html')
            .then(response => response.text())
            .then(data => {
                popupContainer.innerHTML = data;

                selectColor();
                counter();

                resolve();
            })
            .catch(error => {
                console.error(error)

                reject(error)
            });
    })
}

async function loadCategories() {
    return new Promise((resolve, reject) => {

        fetch('/categories.json')
            .then(response => response.json())
            .then(data => {

                data.forEach(category => {
                    categories.push(category);
                });

                resolve()

            })
            .catch(error => {
                console.error(error)

                reject(error)
            });
    })
}

function getCategoryByPageName(pageName) {
    switch (pageName) {
        case 'smartphones.html':
            return 0;
        case 'netbooks.html':
            return 1;
        case 'monitors.html':
            return 2;
        case 'mouses.html':
            return 3;
        case 'cameras.html':
            return 4;
        case 'humidifiers.html':
            return 5;
        default:
            return null;
    }
}

async function renderCategories() {
    return new Promise((resolve) => {

        const categoriesContainer = document.querySelector('.categories');

        categories.forEach(category => {
            const categoryCard = document.createElement('a');
            categoryCard.classList.add('categories__card');

            const categoryNameElem = document.createElement('h2');
            categoryNameElem.classList.add('categories__card-title');

            const btnEffect = document.createElement('span');
            btnEffect.classList.add('categories__btnEffect')

            categoryNameElem.textContent = category.category;
            categoryCard.appendChild(categoryNameElem);

            categoriesContainer.appendChild(categoryCard);
            categoryNameElem.appendChild(btnEffect);

            categoryCard.style.backgroundImage = `url(${category.imagePath})`;
            categoryCard.setAttribute("href", category.pagePath);

        });

        resolve(categories)

    })
}

async function renderCards () {
    return new Promise((resolve, reject) => {

        const cardsContainer = document.querySelector('.catalog-cards__container');
        const pageName = window.location.pathname.split('/').pop();
        const categoryId = getCategoryByPageName(pageName);

        fetch('/data.json')
            .then(response => response.json())
            .then(data => {

                data.forEach(element => {

                    if (element.categoryId === categoryId.toString()) {

                        const catalogCard = document.createElement('div');
                        catalogCard.classList.add('catalog-cards__card');

                        const catalogCardName = document.createElement('h2');
                        catalogCardName.classList.add('catalog-cards__card-title');

                        const catalogCardImage = document.createElement('img');
                        catalogCardImage.classList.add('catalog-cards__card-image');

                        const catalogCardDescription = document.createElement('p');
                        catalogCardDescription.classList.add('catalog-cards__card-description');

                        const catalogCardCount = document.createElement('p');
                        catalogCardCount.classList.add('catalog-cards__card-count');

                        const catalogCardBtn = document.createElement('button');
                        catalogCardBtn.classList.add('catalog-cards__card-btn', 'popup');

                        catalogCardImage.setAttribute("src", element.image);
                        catalogCard.appendChild(catalogCardImage);

                        catalogCardName.textContent = element.name;
                        catalogCard.appendChild(catalogCardName);

                        catalogCardDescription.textContent = element.description;
                        catalogCard.appendChild(catalogCardDescription);

                        catalogCardCount.textContent = `Осталось на складе: ${element.stock}`
                        catalogCard.appendChild(catalogCardCount);

                        catalogCardBtn.textContent = 'Купить';
                        catalogCard.appendChild(catalogCardBtn);
                        catalogCardBtn.setAttribute("onclick", 'showPopup()')

                        cardsContainer.appendChild(catalogCard);

                        if (element.stock === "Закончились :(") {
                            catalogCardBtn.classList.add('disabled');
                            catalogCardBtn.disabled = true;
                        }

                    }

                });

                resolve()

            })
            .catch(error => {
                console.error(error)

                reject(error)
            });
    })
}

function renderHeaderMenu() {
    const headerMenuContainer = document.querySelector('.header__menu-list');
    headerMenuContainer.setAttribute("id", 'menu');

    categories.forEach(category => {
        const headerMenuItem = document.createElement('li');
        headerMenuItem.classList.add('header__menu-item');

        const headerMenuLink = document.createElement('a');
        headerMenuLink.classList.add('header__menu-link');
        headerMenuLink.setAttribute("href", category.pagePath);

        headerMenuLink.textContent = category.category;

        headerMenuItem.appendChild(headerMenuLink);
        headerMenuContainer.appendChild(headerMenuItem);
    });

    const burgerBtn = document.querySelector('.header__burger-menu');

    if (burgerBtn) {
        burgerBtn.addEventListener('click', function () {
            headerMenuContainer.classList.toggle('burger-open');
        });
    }
}

function renderFooterMenu () {
    const footerMenuContainer = document.querySelector('.footer__menu-list');

    categories.forEach(category => {
        const footerMenuItem = document.createElement('li');
        footerMenuItem.classList.add('footer__menu-item');

        const footerMenuLink = document.createElement('a');
        footerMenuLink.classList.add('footer__menu-link');
        footerMenuLink.setAttribute("href", category.pagePath);

        footerMenuLink.textContent = category.category;

        footerMenuItem.appendChild(footerMenuLink);
        footerMenuContainer.appendChild(footerMenuItem);
    });
}

function themeSwitcher () {
    const themeSwitcher = document.getElementById('theme-switcher-input');

    themeSwitcher.addEventListener('change', function() {
        document.body.classList.toggle('dark-theme');
        if (this.checked) {
            document.querySelector('.theme-switcher__text').textContent = 'Светлая тема';
            localStorage.setItem('theme', 'dark');
        } else {
            document.querySelector('.theme-switcher__text').textContent = 'Темная тема';
            localStorage.setItem('theme', 'light');
        }
    });

    const savedTheme = localStorage.getItem('theme');

    if (savedTheme === 'dark') {
        document.body.classList.add('dark-theme');
        themeSwitcher.checked = true;
        document.querySelector('.theme-switcher__text').textContent = 'Светлая тема';
    }
}

function showPopup() {

    const popup = document.querySelector('.order-popup');
    const backdrop = document.querySelector('.modal-backdrop');

    popup.classList.add('active');
    backdrop.classList.add('active');
}

function closePopup () {
    const popup = document.querySelector('.order-popup');
    const backdrop = document.querySelector('.modal-backdrop');

    popup.classList.remove('active');
    backdrop.classList.remove('active');
}

function selectColor() {

    const colorButtons = document.querySelectorAll('.order-popup__color-btn input');

    colorButtons.forEach(function (button) {
        button.addEventListener('change', function () {

            colorButtons.forEach(function (button) {
                button.parentElement.classList.remove('selected');
            });

            this.parentElement.classList.add('selected');
        });
    });

}

function counter () {
    const counterInput = document.querySelector('.order-popup__counter-input');
    const counterMinusButton = document.querySelector('.order-popup__counter-minus');
    const counterPlusButton = document.querySelector('.order-popup__counter-plus');
    let counterValue = 1;

    counterMinusButton.addEventListener('click', function(event) {
        event.preventDefault();
        if (counterValue > 1) {
            counterValue--;
            counterInput.value = counterValue;
        }
    });

    counterPlusButton.addEventListener('click', function(event) {
        event.preventDefault();
        counterValue++;
        counterInput.value = counterValue;
    });
}

function orderSuccess () {
    alert('Заказ успешно оформлен!');
}

window.onscroll = function() {
    const scrollUpBtn = document.querySelector('.scroll-up');
    if (document.documentElement.scrollTop > document.documentElement.clientHeight) {
        scrollUpBtn.style.opacity = "1";
    } else {
        scrollUpBtn.style.opacity = "0";
    }
}

let timeOut;

function scrollUp () {
    let toTop = Math.max(document.body.scrollTop,document.documentElement.scrollTop);
    if(toTop > 0) {
        window.scrollBy(0,-100);
        timeOut = setTimeout('scrollUp()',20);
    } else clearTimeout(timeOut);
}

document.addEventListener('DOMContentLoaded', function () {
    init()
});

async function init() {
    await loadCategories();
    await importHeader();
    await importFooter();
    await importPopup();
    if (window.location.pathname === '/gsm-market/index.html') {
        await renderCategories();
    }else {
        await renderCards();
    }
    renderHeaderMenu();
    themeSwitcher();
    renderFooterMenu();
}

